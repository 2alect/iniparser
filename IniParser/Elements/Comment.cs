﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IniParser.Elements
{
    public class Comment
    {
        private string _text;

        public Comment()
        {
            _text = "";
        }

        public Comment(string text)
        {
            _text = text;
        }

        public string Text
        {
            set
            {
                if (value == null) throw new ArgumentNullException(nameof(value));
                _text = value;
            }
            get { return _text; }
        }
    }
}
