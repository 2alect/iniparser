﻿using System;
using IniParser.Elements.Section;

namespace IniParser.Elements.Option
{
    public class Option : SectionItem
    {
        public Option(string name)
        {
            Name = name;
        }

        public string Name
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public string GenerateTextView()
        {
            throw new NotImplementedException();
        }

        public string Description { set; get; }

        public OptionItem Value { set; get; }
    }
}
