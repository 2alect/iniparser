﻿namespace IniParser.Elements.Option
{
    public abstract class OptionItem : Element
    {
        public abstract string GenerateTextView();
    }
}
