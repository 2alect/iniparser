﻿namespace IniParser.Elements
{
    public interface Element
    {
        string GenerateTextView();
    }
}

