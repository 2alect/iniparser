﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IniParser.Elements.Value
{
    public enum ValueItemType
    {
        Double,
        String,
        Reference
    }

    public abstract class ValueItem
    {
        public static implicit operator double(ValueItem value)
        {
            if (!(value is DoubleValue))
            {
                throw new InvalidCastException();
            }

            return ((DoubleValue)value).Value;
        }

        public static implicit operator ValueItem(double value)
        {
            return new DoubleValue(value);
        }
    }
}
