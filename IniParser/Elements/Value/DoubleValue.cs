﻿namespace IniParser.Elements.Value
{
    public class DoubleValue : ValueItem
    {


        public static implicit operator double(DoubleValue value)
        {
            return value.Value;
        }

        public static implicit operator DoubleValue(double value)
        {
            return new DoubleValue(value);
        }

        public double Value { set; get; }

        public ValueItemType Type => ValueItemType.Double;

        public DoubleValue(double value)
        {
            Value = value;
        }
    }
}
