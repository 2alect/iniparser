﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IniParser.Elements
{
    public class ConfigurationComment : Comment, ConfigurationItem
    {
        public string Name { get; set; }

        public ConfigurationComment(string text) : base(text) { }

        public string GenerateTextView()
        {
            throw new NotImplementedException();
        }
    }
}
