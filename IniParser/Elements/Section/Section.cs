﻿using IniParser.Elements.Option;

namespace IniParser.Elements.Section
{
    public class Section: CompositeElement<SectionItem>, ConfigurationItem
    {
        private string _name;

        public Section()
        {
            _name = "undefined";
        }

        public Section(string name)
        {
            _name = name;
        }
    }
}
