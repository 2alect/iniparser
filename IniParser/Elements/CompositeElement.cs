﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Security.Policy;
using System.Text;

namespace IniParser.Elements
{
    public abstract class CompositeElement<TInnerItem> : Element, IEnumerable<TInnerItem> where TInnerItem : Element
    {
        private readonly List<TInnerItem> _innerItems = new List<TInnerItem>();

        public int Count => _innerItems.Count;

        public string DescriptionHeader { set; get; }

        private string _name;

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                if (String.IsNullOrWhiteSpace(value))
                    throw new ArgumentException("Argument is null or whitespace", nameof(value));

                _name = value;
            }
        }

        public string GenerateTextView()
        {
            var rawText = new StringBuilder();
            _innerItems.ForEach(x => rawText.Append(x.GenerateTextView()).Append("\n"));
            return rawText.ToString();
        }

        public void Add(TInnerItem item)
        {
            _innerItems.Add(item);
        }

        public void Clear()
        {
            _innerItems.Clear();
        }

        public bool Contains(TInnerItem item)
        {
            return _innerItems.Contains(item);
        }

        public void CopyTo(TInnerItem[] array, int arrayIndex)
        {
            _innerItems.CopyTo(array, arrayIndex);
        }

        public bool Remove(TInnerItem item)
        {
            return _innerItems.Remove(item);
        }

        public TInnerItem this[int id]
        {
            get { return _innerItems[id]; }
            set { _innerItems[id] = value; }
        }

        public TInnerItem this[string name]
        {
            get
            {
                foreach (var item in _innerItems)
                {
                    var componentItem = item as CompositeElement<TInnerItem>;
                    if (componentItem == null) continue;

                    if (componentItem.Name == name) return item;
                }

                throw new KeyNotFoundException();
            }
        }

        public IEnumerator<TInnerItem> GetEnumerator()
        {
            return _innerItems.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _innerItems.GetEnumerator();
        }
    }
}
