﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Xml;

using System.Text.RegularExpressions;
using IniParser.Elements;
using IniParser.Elements.Option;
using IniParser.Elements.Section;

namespace IniParser
{
    public class Parser
    {
        public enum ItemState
        {
            Start,
            Configuration,
            ConfigurationDescription,
            ConfigurationComment,
            ConfigurationUnknown,
            Section,
            SectionComment,
            SectionUnknown,
            Option,
            OptionComment,
            OptionUnknown,
            Value
        }

        private string _sectionPattern = @"\s*\[\s*([a-zA-Z\.\$\:]{1}[a-zA-Z0-9_~\-\.\:\$\s]*)\]\s*";
        private string _commentPattern = @";(.*)";
        private string _optionPattern = @"";

        bool TryReadSection(string source, int startat, out Section section, out int endPosition)
        {
            var regex = new Regex(_sectionPattern);
            var result = regex.Match(source, startat);

            if (!result.Success && result.Groups.Count < 2)
            {
                section = null;
                endPosition = startat;
                return false;
            }

            string sectionName = result.Groups[1].Value.Trim();
            section = new Section(sectionName);
            endPosition = startat + result.Groups[0].Value.Length;
            return true;
        }

        bool TryReadCommentText(string source, int startat, out string text, out int endPosition)
        {
            var regex = new Regex(_commentPattern);
            var result = regex.Match(source, startat);

            if (!result.Success && result.Groups.Count < 2)
            {
                text = null;
                endPosition = startat;
                return false;
            }

            text = result.Groups[1].Value;
            endPosition = startat + result.Groups[0].Value.Length;
            return true;
        }

        bool TryReadOption(string source, int startat, out Option option, out int endPosition)
        {
            throw new NotImplementedException();
        }

        bool TryReadUnknown(string source, int startat, out Comment comment, out int endPosition)
        {
            throw new NotImplementedException();
        }

        public Configuration Load(string path)
        {
            var configuration = new Configuration();
            var lastState = ItemState.Configuration;

            var file = new System.IO.StreamReader(path);

            string line;
            int counter = 0;

            Section section;
            Option option;
            Comment comment;
            string descriptionText = "";
            string commentText = "";
            string text = "";

            int endPosition;
            bool lastLineWasSkipped = false;


            while ((line = file.ReadLine()) != null)
            {
                var str = line.Trim();
                if (str.Length == 0)
                {
                    lastLineWasSkipped = true;
                    continue;
                }

                switch (lastState)
                {
                    case ItemState.Start:
                        if (TryReadCommentText(str, 0, out text, out endPosition))
                        {
                            if (lastLineWasSkipped)
                            {
                                commentText = text;
                                lastState = ItemState.ConfigurationDescription;
                            }
                        }
                        else if (TryReadSection(str, 0, out section, out endPosition))
                        {
                            configuration.Add(section);
                            lastState = ItemState.Section;
                        }
                        else if (TryReadUnknown(str, 0, out comment, out endPosition))
                        {
                            throw new NotImplementedException();
                        }
                        break;

                    case ItemState.Section:
                        if (TryReadSection(str, 0, out section, out endPosition))
                        {
                            throw new NotImplementedException();
                        }
                        else if (TryReadComment(str, 0, out comment, out endPosition))
                        {
                            throw new NotImplementedException();
                        }
                        else if (TryReadOption(str, 0, out option, out endPosition))
                        {
                            throw new NotImplementedException();
                        }
                        else if (TryReadUnknown(str, 0, out comment, out endPosition))
                        {
                            throw new NotImplementedException();
                        }
                        break;

                    case ItemState.Option:
                        break;

                    case ItemState.Value:
                        break;
                }


                counter++;
            }

            return configuration;
        }
    }
}
