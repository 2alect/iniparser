﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IniParser.Elements.Section;
using IniParser.Elements.Option;
using IniParser.Elements;
using IniParser;

namespace IniParserExamples
{
    class Program
    {
        public static void ReadingConfiguration()
        {
            //Configuration iniConf = new Configuration();
            //Configuration iniConf = new Parser().Load("../../example.ini");
            //Section section1 = new Section();
            //Option option1 = new Option();
        }

        public static void CreatingConfiguration()
        {
            var iniConf = new Configuration();

            iniConf.DescriptionHeader =   $"{iniConf.Name}.ini\n" +
                                    "It's an example of configuration file in \".ini\" format. The INI file format is an informal\n" +
                                    "standard for configuration files for some platforms or software. INI files are simple text files\n" +
                                    "with a basic structure composed of sections, properties, and values.";

            var confComment = new ConfigurationComment(  "Other information related on configuration file can be described as ConfigurationComment in next\n" +
                                            "several lines.");
            iniConf.Add(confComment);

            for (int i = 0; i < 10; ++i)
            {
                var section = new Section($"Section{i}");

                section.DescriptionHeader =   $"It's an example of comment for the {section.Name}. It will be a good idea to describe here\n" +
                                        "an information about using this section and options that it contains.";

                var sectionComment = new SectionComment("Another information.");
                section.Add(sectionComment);

                for (int j = 0; j < 5; ++j)
                {
                    var option = new Option($"Option{j}");
                    option.Description = $"Information about {section.Name}::{option.Name}.";
                    option.Value = j;
                    section.Add(option);
                }
                iniConf.Add(section);
            }
        }

        static void Main(string[] args)
        {
            
        }
    }
}
